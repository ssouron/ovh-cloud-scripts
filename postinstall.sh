#!/bin/bash

useradd -m -U -s /bin/bash --disabled-password stef
adduser stef sudo
echo "stef ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
cp -r /home/ubuntu/.ssh /home/stef/.ssh
chown -R stef:stef /home/stef/.ssh

deluser ubuntu
rm -rf /home/ubuntu

apt -y update
apt -y upgrade

cd /home/stef
touch config-done
chown stef:stef config-done
